<?php

namespace Drupal\reservation\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Represent various entity events.
 *
 * @see rules_entity_presave()
 */
class DemandeEvent extends Event {

  const DEMANDE_REMOVE = 'reservation.demande.remove';
    
  public function __construct() {
      
  }
  
}
