<?php

namespace Drupal\reservation\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Select;
use Drupal\webform\Element\WebformTermReferenceTrait;
use Drupal\webform\Element\FormatterOptions;

/**
 * Provides a 'webform_schedule_element'.
 *
 * Webform elements are just wrappers around form elements, therefore every
 * webform element must have correspond FormElement.
 *
 * Below is the definition for a custom 'webform_schedule_element' which just
 * renders a simple text field.
 *
 * @FormElement("webform_schedule_element")
 *
 * @see \Drupal\Core\Render\Element\FormElement
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement
 * @see \Drupal\Core\Render\Element\RenderElement
 * @see https://api.drupal.org/api/drupal/namespace/Drupal%21Core%21Render%21Element
 * @see \Drupal\webform_datepicker_element\Element\WebformExampleElement
 */
class WebformScheduleElement extends Select {

  use WebformTermReferenceTrait;


  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#title' => 'choisissez',
      '#vocabulary' => '',
      '#tree_delimiter' => '-',
      '#breadcrumb' => FALSE,
      '#breadcrumb_delimiter' => ' › ',
    ] + parent::getInfo();
  }

  /**
   * {@inheritdoc}
   */
  public static function processSelect(&$element, FormStateInterface $form_state, &$complete_form) {
    static::setOptions($element);

    $element = parent::processSelect($element, $form_state, $complete_form);

    // Must convert this element['#type'] to a 'select' to prevent
    // "Illegal choice %choice in %name element" validation error.
    // @see \Drupal\Core\Form\FormValidator::performRequiredValidation
    $element['#id'] = 'scheduleElement';
    $element['#type'] = 'select';
    $element['#title'] = 'choisissez';
    $element['#attached']['library'][] = 'reservation/reservation.schedule';

    return $element;
  }

}

