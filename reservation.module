<?php

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\RouteMatchInterface;

use Drupal\node\Entity\Node;
use Drupal\reservation\Entity\ReservationRessource;
use Drupal\Core\Entity\EntityInterface;
use Drupal\reservation\Event\DemandeEvent;
use Drupal\reservation\Event\EntityEvent;
use Drupal\webform\Entity\WebformSubmission;
use Drupal\Core\Form\FormStateInterface;


/**
 * @file
 * Contains Drupal\reservation\reservation.module.
 */

/**
 * Implements hook_toolbar().
 */
function reservation_toolbar() {
  $items = [];

  $items['reservation'] = [
    '#type' => 'toolbar_item',
    '#attached' => [
      'library' => [
        'reservation/drupal.reservation.toolbar',
      ],
    ],
  ];

  return $items;
}

/**
 * Implements hook_entity_update().
 */
function reservation_entity_update($entity) 
{
  $queue = \Drupal::queue('reservation_tasks_entity_updates');
  $data = [ 'entity_type' => $entity->getEntityType(), 'id' => $entity->id()]; 
  $queue->createItem($data);
}

/**
 * Implements hook_cron().
 * 
 * Suppression des demandes dépassant une période donnée.
 * Envoi des mails automatique pour les demandes confirmées.
 * 
 */
function reservation_cron() {
    
    // Suppression des demandes dépassant une période donnée
    $queue = \Drupal::queue('reservation_demande_remove');
    $queue->createItem(null);
    
    // Envoi des mails automatique pour les demandes confirmées
    $queue = \Drupal::queue('reservation_mail');
    $queue->createItem(null);    
}

/*
 * 
 */
function reservation_node_insert(EntityInterface $entity) 
{    
  \Drupal::service('event_dispatcher')->dispatch(EntityEvent::NODE_SAVE, new EntityEvent($entity));
}

/**
 * 
 */
function reservation_node_update(EntityInterface $entity) 
{    
  \Drupal::service('event_dispatcher')->dispatch(EntityEvent::NODE_SAVE, new EntityEvent($entity));
}

function reservation_entity_type_alter(array &$entity_types) {
  $entity_types['user']
          ->setFormClass('default', 'Drupal\reservation\Form\ReservationProfileForm')
          ->setFormClass('edit', 'Drupal\reservation\Form\ReservationProfileForm');
}

function reservation_webform_element_alter(array &$element, FormStateInterface $form_state, array $context) 
{    
    $webform_reservation = $form_state->getValue('webform_reservation');
    if($webform_reservation && $form_state->getStorage()['current_page'] == 'page_2')
    {        
        $reservationDemande = \Drupal::service('reservation.demande');        
    }    
}

/**
 * Implements hook_theme().
 */
function reservation_theme() {
  return [

    'template_parametre_notification' => [
      'variables' => [
        'form_select' => NULL,
        'form_statut' => NULL,
        'form_filter' => NULL,
        'form_email' => NULL,
        'rows' => NULL,
        'tabs' => NULL,
        'type_email' => NULL,    
        'nid' => NULL,          
      ],
    ],
      
    'template_demande_caution' => [
      'variables' => [
        'form' => NULL,
      ],
    ],
      
    'template_disponibilite_liste' => [
      'variables' => [
        'tab_diponinilite' => NULL,
        'form' => NULL,
        'form_caution' => NULL,
        'rows' => NULL,
        'year' => NULL,
      ],
    ],

    'template_disponibilite_date' => [
      'variables' => [
        'tab_diponinilite' => NULL,
        'form_select' => NULL,
        'form' => NULL,
      ],
    ],
      
    'template_disponibilite_horaire' => [
      'variables' => [
        'tab_diponinilite' => NULL,
        'form_select' => NULL,
        'form' => NULL,
        'tabs' => NULL,
        'rows' => NULL,
        'nid' => NULL,
        'year' => NULL,
      ],
    ],
      
  ];
}

/**
 * Implements hook_mail().
 */
function reservation_mail($key, &$message, $params) 
{
  switch ($key) {
    case 'reservation_demande_mail':
      $message['from'] = $params['from'];
      $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed; delsp=yes';
      $message['subject'] = $params['subject'];
      $message['body'][] = $params['message'];
      $message['options'] = [];
      if (isset($params['options']) && !empty($params['options'])) {
        foreach ($params['options'] as $key => $value) {
          $message['options'][$key] = $value;
        }
      }
      break;
  }
}


/**
 * Implements hook_help().
 */
function reservation_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.reservation':
      $output = '';
      $output .= '<h3>Description</h3>';
      $output .= '<p>Le module réservation, permet de créer des réservations liées à un node.</p>';

      return $output;
  }
}
